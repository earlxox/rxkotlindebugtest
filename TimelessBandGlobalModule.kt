package com.wondercise.wondercisetimelessband

object TimelessBandGlobalModule {

    var memberId: String           = ""
    var scanFiliter: Boolean       = false
    var confidenceHeartRate: Int   = 0
    var wornHeartRate: Boolean     = false
    var confidenceSpo2: Int       = 0
    var wornSpo2: Boolean          = false

    /**
     * timelessBandSetMemberId()
     * @param memberId
     * @param scanFiliter

     */
    fun timelessBandSetMemberId(memberId: String, scanFiliter: Boolean) {
        this.memberId = memberId
        this.scanFiliter = scanFiliter
    }

    /**
     * timelessBandHeartRateFilter()
     * @param confidence
     * @param worn

     */
    fun timelessBandHeartRateFilter(confidence: Int, worn: Boolean) {
        this.confidenceHeartRate = confidence
        this.wornHeartRate = worn
    }

    /**
     * timelessBandSpo2Filter()
     * @param confidence
     * @param worn

     */
    fun timelessBandSpo2Filter(confidence: Int, worn: Boolean) {
        this.confidenceSpo2 = confidence
        this.wornSpo2 = worn
    }
}