package com.wondercise.wondercisetimelessband

import android.bluetooth.*
import android.content.Context
import android.content.Intent
import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import com.android.core.Utils
import com.wondercise.device.FitnessDevice
import com.wondercise.wondercisetimelessband.callback.ButtonDetectCallback
import com.wondercise.wondercisetimelessband.callback.UserInformationCallback
import com.wondercise.wondercisetimelessband.managers.*
import com.wondercise.wondercisetimelessband.suota.File.getByFirstFileName
import com.wondercise.wondercisetimelessband.suota.Statics
import com.wondercise.wondercisetimelessband.suota.SuotaManager
import com.wondercise.wondercisetimelessband.util.*
import io.reactivex.*
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import io.reactivex.subjects.ReplaySubject
import java.nio.ByteBuffer
import java.nio.ByteOrder
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.concurrent.schedule
import kotlin.math.min

private const val TAG = "timelessHelper"

sealed interface TimelessBandDeviceField {
    data class battery(val batteryPercentage: Int) : TimelessBandDeviceField
    data class rssi(val signalStrength: Int) : TimelessBandDeviceField
    data class firmwareVersion(val firmwareVersion: String) : TimelessBandDeviceField
}
class TimelessBandBleAlgorithmHelper(
    private val mContext: Context,
    private val mConnectionStateSubject: BehaviorSubject<FitnessDevice.ConnectionState>,
    private val mBluetoothDevice: BluetoothDevice,
    private val canConnectGatt: Boolean
) {
    val onConnectedDeviceFieldSubject = PublishSubject.create<TimelessBandDeviceField>()


    private var mBluetoothGatt: BluetoothGatt? = null
    //Command UUID
    val DEVICE_S_DATA_SERVICE_UUID: UUID = UUID.fromString("0000ff06-0000-1000-8000-00805f9b34fb")
    val DEVICE_S_DATA_NOTIFY_CHARACTERISTIC_UUID = UUID.fromString("0000ff08-0000-1000-8000-00805f9b34fb")
    val DEVICE_S_WRITE_CHARACTERISTIC_UUID = UUID.fromString("0000ff07-0000-1000-8000-00805f9b34fb")

    val DEVICE_DATA_SERVICE_UUID = UUID.fromString("0000ff10-0000-1000-8000-00805f9b34fb")
    val DEVICE_DATA_NOTIFY_CHARACTERISTIC_UUID = UUID.fromString("0000ff11-0000-1000-8000-00805f9b34fb")
    val DEVICE_COMMAND_WRITE_CHARACTERISTIC_UUID = UUID.fromString("0000ff12-0000-1000-8000-00805f9b34fb")
    val DEVICE_STATUS_WRITE_CHARACTERISTIC_UUID = UUID.fromString("0000ff13-0000-1000-8000-00805f9b34fb")
    val DEVICE_STATUS_NOTIFY_CHARACTERISTIC_UUID = UUID.fromString("0000ff14-0000-1000-8000-00805f9b34fb")
    val DEVICE_FILE_NAME_NOTIFY_CHARACTERISTIC_UUID = UUID.fromString("0000ff15-0000-1000-8000-00805f9b34fb")
    val DEVICE_CRC32_NOTIFY_CHARACTERISTIC_UUID = UUID.fromString("0000ff16-0000-1000-8000-00805f9b34fb")
    val DEVICE_FILE_DATETIME_NOTIFY_CHARACTERISTIC_UUID = UUID.fromString("0000ff17-0000-1000-8000-00805f9b34fb")
    val DEVICE_REAL_TIME_NOTIFY_CHARACTERISTIC_UUID = UUID.fromString("0000ff18-0000-1000-8000-00805f9b34fb")
    val DEVICE_STORAGE_INFO_NOTIFY_CHARACTERISTIC_UUID = UUID.fromString("0000ff19-0000-1000-8000-00805f9b34fb")
    val DES_CLIENT_UUID = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb")

    //register callback
    private val lock = Object()
    protected lateinit var mIMUSubject: BehaviorSubject<RawIMUData>
    protected lateinit var mCharacterWriteEmitter: ObservableEmitter<UUID>
    protected val mHeartRateSubject: ReplaySubject<HeartRateDetect> = ReplaySubject.createWithTime(8, TimeUnit.SECONDS, Schedulers.io())
    protected val mSpo2Subject: ReplaySubject<Spo2Detect> = ReplaySubject.createWithTime(8, TimeUnit.SECONDS, Schedulers.io())
    val batteryUsage: BehaviorSubject<BatteryUsage> = BehaviorSubject.create()
    val storageUsage: BehaviorSubject<StorageUsage> = BehaviorSubject.create()
    val workingStatus: BehaviorSubject<WorkingStatus> = BehaviorSubject.create()
    val sensorStatus: BehaviorSubject<SensorStatus> = BehaviorSubject.create()
    val manualHeartRateSPO2: BehaviorSubject<HeartRateSPO2> = BehaviorSubject.create()

    private val mOperationQueue: MutableList<UUID> = mutableListOf()

    //OTA
    val suotaManager = SuotaManager()
    var isOTA = false

    var lowBatteryUsage: BehaviorSubject<BatteryUsage?>                 = BehaviorSubject.create()
    var lowStorageUsage: BehaviorSubject<StorageUsage?>                 = BehaviorSubject.create()
    var gattSyncStatus: BehaviorSubject<GattSyncStatus>                 = BehaviorSubject.createDefault(GattSyncStatus.STATE_IDLE)
    var imuModeTransmitting: BehaviorSubject<Boolean>                   = BehaviorSubject.createDefault(false)
    var firmwareUpgrading: BehaviorSubject<Boolean>                     = BehaviorSubject.createDefault(false)
    var currentExercise: BehaviorSubject<CurrentExercise?>              = BehaviorSubject.create()
    var heartRateDetectObservable: PublishSubject<HeartRateRecord>      = PublishSubject.create<HeartRateRecord>()
    var spo2DetectObservable: PublishSubject<Spo2Record>                = PublishSubject.create<Spo2Record>()
    var heartRateMonitoringObservable: PublishSubject<HeartRateRecord>  = PublishSubject.create<HeartRateRecord>()
    var spo2MonitoringObservable: PublishSubject<Spo2Record>            = PublishSubject.create<Spo2Record>()

    var mHeartRateRecordType: HeartRateRecord.HeartRateRecordType =  HeartRateRecord.HeartRateRecordType.AUTO
    var mSpo2RecordType: Spo2Record.Spo2RecordType =  Spo2Record.Spo2RecordType.AUTO
    var haveAutoIt = false
    var haveButtonIt = false

    private val mBluetoothGattCallback = object : BluetoothGattCallback() {
        override fun onServicesDiscovered(gatt: BluetoothGatt?, status: Int) {
            Log.d(TAG, "onServicesDiscovered: ${status.toString()}")
            scheduleRssiReadRequest()
            //scheduleBatteryReadRequest()

            gatt?.getService(DEVICE_DATA_SERVICE_UUID)?.let { service ->
                service.getCharacteristic(DEVICE_DATA_NOTIFY_CHARACTERISTIC_UUID)
                    ?.let {
                        enableNotifyCharacteristic(gatt, it, DES_CLIENT_UUID)
                    }
            } ?: let {
                Log.d(TAG,"服務獲取失敗  斷開連線")
                gatt?.disconnect()
            }
        }

        override fun onMtuChanged(gatt: BluetoothGatt?, mtu: Int, status: Int) {
            super.onMtuChanged(gatt, mtu, status)
            Log.d(TAG,"MTU更新成功... mtu:$mtu")
            initStatus()
        }

        override fun onDescriptorWrite(
            gatt: BluetoothGatt?,
            descriptor: BluetoothGattDescriptor?,
            status: Int,
        ) {
            descriptor?.value?.let {
                if (Arrays.equals(it, BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE)
                    && status != BluetoothGatt.GATT_SUCCESS) {
                    Log.d(TAG, "某个notify特征初始化失败，断开设备连接! ${descriptor.characteristic.uuid}")
                    gatt?.disconnect()
                    return
                }
            }

            descriptor?.characteristic?.service?.uuid?.let { serviceUUID ->
                if (serviceUUID == DEVICE_DATA_SERVICE_UUID) {
                    gatt?.getService(DEVICE_DATA_SERVICE_UUID)?.let { service ->
                        descriptor.characteristic?.uuid?.let {
                            gatt.setCharacteristicNotification(
                                service.getCharacteristic(it),
                                true
                            )
                            if (it == DEVICE_FILE_DATETIME_NOTIFY_CHARACTERISTIC_UUID) {
                                Log.d(TAG,"设备所有的notify服务已经初始化完成")
                                enableNotifyCharacteristic(gatt,
                                    gatt.getService(DEVICE_S_DATA_SERVICE_UUID).getCharacteristic(DEVICE_S_DATA_NOTIFY_CHARACTERISTIC_UUID),
                                    DES_CLIENT_UUID
                                )
                            } else {
                                val uuid = when (it) {
                                    DEVICE_DATA_NOTIFY_CHARACTERISTIC_UUID ->
                                        DEVICE_FILE_NAME_NOTIFY_CHARACTERISTIC_UUID
                                    DEVICE_FILE_NAME_NOTIFY_CHARACTERISTIC_UUID ->
                                        DEVICE_STATUS_NOTIFY_CHARACTERISTIC_UUID
                                    DEVICE_STATUS_NOTIFY_CHARACTERISTIC_UUID ->
                                        DEVICE_REAL_TIME_NOTIFY_CHARACTERISTIC_UUID
                                    DEVICE_REAL_TIME_NOTIFY_CHARACTERISTIC_UUID ->
                                        DEVICE_CRC32_NOTIFY_CHARACTERISTIC_UUID
                                    DEVICE_CRC32_NOTIFY_CHARACTERISTIC_UUID ->
                                        DEVICE_STORAGE_INFO_NOTIFY_CHARACTERISTIC_UUID
                                    DEVICE_STORAGE_INFO_NOTIFY_CHARACTERISTIC_UUID ->
                                        DEVICE_FILE_DATETIME_NOTIFY_CHARACTERISTIC_UUID
                                    else -> null
                                }
                                uuid?.let {
                                    enableNotifyCharacteristic(gatt, service.getCharacteristic(it), DES_CLIENT_UUID)
                                }
                            }
                        }
                    }
                } else if (serviceUUID == DEVICE_S_DATA_SERVICE_UUID) {
                    gatt?.getService(DEVICE_S_DATA_SERVICE_UUID)?.let { service ->
                        descriptor.characteristic?.uuid?.let {
                            gatt.setCharacteristicNotification(
                                service.getCharacteristic(it),
                                true
                            )
                            if (it == DEVICE_S_DATA_NOTIFY_CHARACTERISTIC_UUID) {
                                Log.d(TAG,"设备所有的notify服务已经初始化完成")
                                mConnectionStateSubject.onNext(FitnessDevice.ConnectionState.connected)

                                gatt.requestMtu(512)

                            }
                        }
                    }
                } else {

                }

            } ?: let {
                Log.d(TAG, "characteristic notify enable fail! ${descriptor?.characteristic?.uuid}")
                gatt?.disconnect()
            }

            descriptor?.characteristic?.uuid?.let {
                if (it == Statics.SPOTA_SERV_STATUS_UUID)
                    suotaManager.processStep(2)
            }

        }

        override fun onConnectionStateChange(gatt: BluetoothGatt?, status: Int, newState: Int) {
            Log.d(TAG, "onConnectionStateChange $status-$newState")
            when (newState) {
                BluetoothGatt.STATE_CONNECTED -> {
                    Log.d(TAG,"设备已连接...准备初始化服务...")
                    gatt?.discoverServices()
                    mConnectionStateSubject.onNext(FitnessDevice.ConnectionState.connecting)
                }
                BluetoothGatt.STATE_DISCONNECTED -> {
                    Log.d(TAG,"设备已断开连接... reason:$status")
                    isOTA = false
                    mConnectionStateSubject.onNext(FitnessDevice.ConnectionState.disconnected)
                }
            }
        }

        @RequiresApi(Build.VERSION_CODES.O)
        override fun onCharacteristicChanged(
            gatt: BluetoothGatt?,
            characteristic: BluetoothGattCharacteristic?
        ) {
            if (!isOTA) {
                characteristic?.let {
                    onReceiveDeviceData(gatt, it, it.value)
                }
            } else {
                // SUOTA image started

                characteristic?.let {
//                    Log.d(TAG, "isOTA onReceive uuid: ${characteristic.uuid}")
//                    Log.d(TAG, "isOTA onReceive value: ${Utils.byteArrayToHexStrWithSpaceSplit(characteristic.value)}")
                }

                val value = characteristic!!.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 0)

                var step = -1
                var error = -1
                var status = -1
                val isSuota = suotaManager.type == SuotaManager.TYPE

                if (value == 0x10) {
                    step = 3
                } else if (value == 0x02) {
                    step = if (isSuota) 5 else 8
                } else if (!isSuota && (value == 0x01 || value == 0x03)) {
                    status = value
                } else {
                    error = value
                }
                if (step >= 0 || error >= 0 || status >= 0) {
                    val intent = Intent()
                    intent.action = Statics.BLUETOOTH_GATT_UPDATE
                    intent.putExtra("step", step)
                    intent.putExtra("error", error)
                    intent.putExtra("status", status)
                    suotaManager.processStep(step)
                }
            }
        }

        override fun onCharacteristicWrite(gatt: BluetoothGatt?,
                                           characteristic: BluetoothGattCharacteristic?,
                                           status: Int
        ) {
            if (!isOTA) {
                characteristic?.uuid?.let {
                    if (status == 0) {
                        mCharacterWriteEmitter.onNext(it)
                    }
                }
            } else {
                if (characteristic?.uuid == Statics.SPOTA_MEM_DEV_UUID) {
                    suotaManager.processStep(3)
                }
                // Step 3 callback: write SPOTA_GPIO_MAP_UUID value
                else if (characteristic?.uuid == Statics.SPOTA_GPIO_MAP_UUID) {
                    suotaManager.processStep(4)
                } else if (characteristic?.uuid == Statics.SPOTA_PATCH_LEN_UUID) {
                    suotaManager.processStep(5)
                } else if (characteristic?.uuid == Statics.SPOTA_PATCH_DATA_UUID && suotaManager.chunkCounter != -1) {
                    if (suotaManager.sendBlock() != 100.toFloat()) {
                        firmwareUpgrading.onNext(false)
                    } else {
                        firmwareUpgrading.onNext(true)
                    }
                }
            }
        }

        override fun onReadRemoteRssi(gatt: BluetoothGatt, rssi: Int, status: Int) {
            if (status != BluetoothGatt.GATT_SUCCESS) {
                return
            }
            onConnectedDeviceFieldSubject.onNext(TimelessBandDeviceField.rssi(rssi))
        }


    }

    init {
        Log.d(TAG, "TimelessBandBleAlgorithmHelper init id: ${mBluetoothDevice.address} ")

        connectDevice()

        val characterWriteDisposable = Observable.create<UUID> {
            mCharacterWriteEmitter = it
        }.subscribe {
            if (mOperationQueue.size != 0) {
                if (it == DEVICE_COMMAND_WRITE_CHARACTERISTIC_UUID) {
                    notifyStatus(byteArrayOf(0x10))
                }
                mOperationQueue.removeAt(0)
            }
        }
    }

    internal fun onBluetoothAdapterEnabled(enabled: Boolean) {
        if (enabled) {
            Log.d(TAG, "onBluetoothAdapterEnabled 重新連線 +++")
            mBluetoothGatt?.close()
            mBluetoothGatt = mBluetoothDevice.connectGatt(mContext,true, mBluetoothGattCallback)
        } else {
            closeBleSession()
            mBluetoothGatt?.disconnect()
        }
    }

    fun enableNotifyCharacteristic(
        gatt: BluetoothGatt,
        characteristic: BluetoothGattCharacteristic,
        descUUID: UUID
    ) {
        characteristic.getDescriptor(descUUID)?.let {
            it.value = BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE
            gatt.writeDescriptor(it)
        }
    }


    //================ GATT Operation ===================================
    private fun sendCommand(byteArray: ByteArray) {
        debugLogCharacteristicValue(TAG + " sendCommand: ", byteArray)
        val cmd = String.format("%02X", byteArray[0])
        synchronized(lock) {
            var beginIndex = 0
            do {
                val pkgSize = min(512, byteArray.size - beginIndex)
//                Log.d(TAG, "sendCommand mtu$mtu, pkgSize:$pkgSize")
                val data = byteArray.slice(beginIndex until (beginIndex + pkgSize)).toByteArray()
                enqueueOperation(
                    data,
                    DEVICE_DATA_SERVICE_UUID,
                    DEVICE_COMMAND_WRITE_CHARACTERISTIC_UUID
                )
                beginIndex += pkgSize
                Log.d(TAG, "sendCommand, cmd[$cmd]: beginIndex:$beginIndex, data:${data.size}")
            } while (beginIndex != byteArray.size)
        }
    }

    fun sendCommand2(byteArray: ByteArray) {
        enqueueOperation(
            byteArray,
            DEVICE_S_DATA_SERVICE_UUID,
            DEVICE_S_WRITE_CHARACTERISTIC_UUID
        )
    }

    private fun notifyStatus(byteArray: ByteArray) {
        enqueueOperation(
            byteArray,
            DEVICE_DATA_SERVICE_UUID,
            DEVICE_STATUS_WRITE_CHARACTERISTIC_UUID
        )
    }

    private fun enqueueOperation(
        byteArray: ByteArray,
        serviceUUID: UUID,
        uuid: UUID
    ) {
        Log.d(TAG, "write uuid:$uuid ,value:${byteArray.toHex()}}")
        mOperationQueue.add(uuid)
        mBluetoothGatt?.getService(serviceUUID)
            ?.getCharacteristic(uuid)
            ?.let {
                it.value = byteArray
                it.writeType
                mBluetoothGatt?.writeCharacteristic(it)
            }
    }

    private fun debugLogCharacteristicValue(tag: String, bytes: ByteArray) {
        val builder = StringBuilder()
        for (byte in bytes) {
            val value = String.format("%02X", byte)
            builder.append(value).append(" ")
        }
        Log.d(tag, builder.toString())
    }

    //==========  Rssi  =====================
    private val DEFAULT_RSSI_READ_PERIOD_SECONDS = 10L

    private var mRssiIntervalDisposable: Disposable? = null

    private fun scheduleRssiReadRequest() {
        stopScheduleRssiReadRequest()
        mRssiIntervalDisposable = Observable
            .interval(DEFAULT_RSSI_READ_PERIOD_SECONDS, TimeUnit.SECONDS)
            .subscribe {
                mBluetoothGatt?.readRemoteRssi()
            }
    }

    private fun stopScheduleRssiReadRequest() {
        mRssiIntervalDisposable?.dispose()
    }

    //=============== bluetooth connect ==============

    private fun connectDevice() {
        if (canConnectGatt) {
            Log.d(TAG,"正在連接")
            mBluetoothGatt = mBluetoothDevice.connectGatt(mContext, true, mBluetoothGattCallback)
        }
    }

    fun unpair() = Completable.create {
        closeBleSession()
        mBluetoothGatt?.close()
        mBluetoothGatt?.disconnect()
        it.onComplete()
    }

    private fun closeBleSession() {
        stopScheduleRssiReadRequest()
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun onReceiveDeviceData(
        gatt: BluetoothGatt?,
        characteristic: BluetoothGattCharacteristic,
        value: ByteArray
    ) {
        characteristic.service?.uuid?.let { serviceUUID ->
            Log.d(TAG, "onReceiveDeviceData uuid: ${characteristic.uuid}")
            Log.d(TAG, "onReceiveDeviceData value: ${value.toHex()}")

            if (serviceUUID == DEVICE_DATA_SERVICE_UUID) {
                when (characteristic.uuid) {
                    DEVICE_REAL_TIME_NOTIFY_CHARACTERISTIC_UUID -> {
                        ExerciseModeDecode(mIMUSubject, value)
                    }
                    DEVICE_STORAGE_INFO_NOTIFY_CHARACTERISTIC_UUID -> {
                        //onReceivedDeviceStorageInfo(value)
                    }
                    else -> {

                    }
                }
            } else if (serviceUUID == DEVICE_S_DATA_SERVICE_UUID && characteristic.uuid == DEVICE_S_DATA_NOTIFY_CHARACTERISTIC_UUID) {
                val command: Int = value[0].toInt().and(0xff)
                when (command) {
                    0x00 -> {

                    }
                    0x1A -> {
                        val unixtime = ByteBuffer.wrap(value.sliceArray(1..4)).order(ByteOrder.BIG_ENDIAN).long
                        val mode = value[5].toInt()
                        val wear = (value[6].toInt().and(0x01) == 0x01)
                        Log.d(TAG, "0x1A, mode= ${mode}")
                        when(mode) {
                            3 -> {
                                connectDevice()
                                val hrConf = value[7].toInt()
                                val hr = value[10].toInt()
//                                mHeartRateSubject.onNext(
                                manualHeartRateSPO2.onNext(
                                    HeartRateSPO2(unixtime, hr, hrConf,0 ,0)
//                                    HeartRateDetect(System.currentTimeMillis(), skin, hr, hr_conf, rri, rri_conf)
                                )
                                heartRateDetectObservable.onNext(HeartRateRecord(Date(unixtime), hr, HeartRateRecord.HeartRateRecordType.AUTO))
                            }
                            4 -> {
                                val hrConf = value[7].toInt()
                                val spo2Conf = value[8].toInt()
                                val hr = value[10].toInt()
                                val spo2 = ByteBuffer.wrap(value.sliceArray(11..12)).order(ByteOrder.BIG_ENDIAN).short.toInt()
                                manualHeartRateSPO2.onNext(HeartRateSPO2(unixtime, hr, hrConf, spo2, spo2Conf))
                                spo2DetectObservable.onNext(Spo2Record(Date(unixtime), hr, Spo2Record.Spo2RecordType.AUTO))
//                                mSpo2Subject.onNext(
//                                    Spo2Detect(System.currentTimeMillis(), skin, spo2, spo2Conf, rri, rri_conf,
//                                        lowSignalQualityFlag, motionFlag, lowPiFlag, unreliableRFlag)
//                                )
                            }
                            else -> {
                                Log.e(TAG, "0x1A, mode= ${mode}")
                            }
                        }
                    }
                    0xF1 -> {
                        //log输出
                        Log.e(TAG, "received device log")
//                        gatt?.device?.address?.let {
//                            dealLogReceived(value.slice(1 until value.size).toByteArray(), it)
//                        }
                    }
                    0x10 -> {
                        onConnectedDeviceFieldSubject.onNext(TimelessBandDeviceField.battery(value[1].toInt()))
                        getBatteryEmitter.onComplete()
                        Log.e(TAG, "received device battery: ${value[1].toInt()}")
                    }
                    0x15 -> {
                        onReceviedGeneralStatus(value.sliceArray(1 until value.size))
                    }
                    0x1B -> {
                        //睡眠數據報告
                        mSleepReportManager.onReceivedSleepData(value.sliceArray(1 until value.size))
                    }
                    0x1C -> {
                        //運動紀錄報告
                        mWorkoutReportManager.onReceivedWorkoutReportData(value.sliceArray(1 until value.size))
                    }
                    0x1D -> {
                        //日常數據報告
                        mDailyReportManager.onReceivedDailyReportData(value.sliceArray(1 until value.size))
                    }
                    0x21 -> {
                        //设备采集器主动下发电量和存储信息
                        val batteryLevel = value[1].toInt().and(0xff)
                        val batteryPercentage = value[2].toInt().and(0xff)
                        val storageLevel = value[3].toInt().and(0xff)
                        val storagePercentage = value[4].toInt().and(0xff)

                        batteryUsage.onNext(BatteryUsage(batteryLevel,batteryPercentage))
                        storageUsage.onNext(StorageUsage(storageLevel,storagePercentage))
                        onConnectedDeviceFieldSubject.onNext(
                                TimelessBandDeviceField.battery(batteryPercentage)
                        )
                        if (batteryPercentage < 20) {
                            lowBatteryUsage.onNext(BatteryUsage(batteryLevel,batteryPercentage))
                        } else {}
                        if (storagePercentage < 7) {
                            lowStorageUsage.onNext(StorageUsage(storageLevel,storagePercentage))
                        } else {}

                    }
                    0x24 -> {
                        //24小时心率数据报告
//                        mHeartRatesManager.onReceivedHeartRateRecords(value)
                        mHeartRateCurve24Manager.onReceivedHeartRateCurve24(value)
                    }
                    0x25 -> {
                        val version = "${value[2]}${value[3]}${value[4]} "
                        onConnectedDeviceFieldSubject.onNext(TimelessBandDeviceField.firmwareVersion(version))
                        getFirmwareVersionSingleEmitter.onSuccess(version)
                    }
                    0x28 -> {
                        Log.e("WTRC", "received 0x28 data, decode is disabled")
                        mButtonDetectReportManager.onReceivedButtonHeartRate_Spo2DetectData(value.sliceArray(1 until value.size))
                    }
                    0x2C -> {
                        val type_byte1 = value[1].toInt().and(0xff)
                        val status_byte2 = value[2].toInt().and(0xff)
                        val status_byte3 = value[3].toInt().and(0xff)
                        var status = SleepSpo2MonitorStatus.SLEEP_MONITOR_ON

                        if (type_byte1 == 0x01) {
                            if (status_byte2 == 1 && status_byte3 == 0) {
                                status = SleepSpo2MonitorStatus.SLEEP_MONITOR_ON
                            } else if (status_byte2 == 0 && status_byte3 == 0) {
                                status = SleepSpo2MonitorStatus.SLEEP_MONITOR_OFF
                            } else if (status_byte2 == 1 && status_byte3 == 1){
                                status = SleepSpo2MonitorStatus.SLEEP_SPO2_MONITOR_ON
                            }
                            mSleepSpo2StatusSingleEmitter.onSuccess(status)
                        } else {
                            Log.e(TAG, "need add GATT profile")
                        }
                    }
                    0x2D -> {
                        //獲取24小時心率設置狀態
                        val type = value[1].toInt()
                        val mode = value[2].toInt()
                        if (type == 0x01){
                            getHeartRateCurve24Emitter.onSuccess(HeartRateMonitorStatus.from(mode))
                        }else{}
                    }
                    0x1E -> {
                        onReceivedLoginStatus(value.sliceArray(1 until value.size))
                    }
                    0x1F -> {
                        onReceivedUserInfo(value.sliceArray((0..10)))
                    }
                    0xFD -> {
                        // 获取重启时间
                        onReceivedLastUptime(value.sliceArray(1..4))
                    }
                    else -> {

                    }
                }
            } else {

            }
        }

    }

    fun initStatus() {
        // getBattery().subscribe { getFirmware().subscribe ({setCurrentTime()},{})  }
    }

    fun reset() {
        sendCommand2(byteArrayOf(0x19))
    }

    private lateinit var getBatteryEmitter: CompletableEmitter
    fun getBattery(): Completable {
        return Completable.create {
            getBatteryEmitter = it
            sendCommand2(byteArrayOf(0x10))
        }
    }

    private lateinit var getFirmwareVersionSingleEmitter: SingleEmitter<String>
    fun getFirmware(): Single<String> {
        return Single.create {
            getFirmwareVersionSingleEmitter = it
            sendCommand2(byteArrayOf(0x25))
        }
    }

    fun setUserInformation(userInfoData: UserInfoData) {
        Log.d(TAG, "prepare to set User Info : $userInfoData")
        val year1 = userInfoData.year.and(0xff).toByte()
        val year2 = userInfoData.year.shr(8).toByte()
        sendCommand2(byteArrayOf(0x1F, 0x01,
            userInfoData.height.toByte(),
            userInfoData.weight.toByte(),
            0x00,
            userInfoData.gender.toByte(),
            year2, year1,
            userInfoData.month.toByte(),
            userInfoData.day.toByte()))
    }

    private lateinit var mUserInformationCallback: UserInformationCallback
    fun getUserInformation(userInformationCallback: UserInformationCallback) {
        mUserInformationCallback = userInformationCallback
        sendCommand2((byteArrayOf(0x1F, 0x02)))
    }

    private lateinit var mUserInfoSingleEmitter: SingleEmitter<UserInfoData>
    fun getUserInfo(): Single<UserInfoData> {
        return Single.create {
            mUserInfoSingleEmitter = it
            sendCommand2(byteArrayOf(0x1F, 0x02))
        }
    }
    private fun onReceivedUserInfo(data: ByteArray) {
        val uif = UserInfoData(
            data[2].toUByte().toInt(), data[3].toUByte().toInt(), data[5].toInt(),
            year = ByteBuffer.wrap(byteArrayOf(data[6], data[7])).order(ByteOrder.BIG_ENDIAN).short.toInt(),
            data[8].toInt().and(0xff), data[9].toInt().and(0xff)
        )
        val debugMSG = "UserInfo = $uif"
        Log.d(TAG, debugMSG)
        mUserInfoSingleEmitter.onSuccess(uif)
    }

    fun setCurrentTime(){
        val timestamp = System.currentTimeMillis()
//        val buffer: ByteBuffer = ByteBuffer.allocate(10)
//        buffer.put(0x12)
//        buffer.putLong(timestamp).rewind()
//        buffer.put(Utils.getCurrentTimeZone().toByte())
//
//        sendCommand2(buffer.array())

        sendCommand2(ByteArray(10) {
            when (it) {
                0 -> 0x12
                in 1..8 -> (timestamp shr 8 * (8 - (it - 1) - 1) and 0xff).toByte()
                9 -> Utils.getCurrentTimeZone().toByte()
                else -> 0
            }
        })
    }

    /**
     * ======================================================================================================
     * Sensor
     * IMU 、 Heart Rate、Spo2
     *
     */

    fun getHeartRateSubject(): BehaviorSubject<HeartRateSPO2> {
//        return mHeartRateSubject
        return manualHeartRateSPO2
    }

    fun getSpo2Subject(): BehaviorSubject<HeartRateSPO2> {
//        return mSpo2Subject
        return manualHeartRateSPO2
    }

    fun startHeartRateDetect(): BehaviorSubject<HeartRateSPO2> {
        //[type], [0x01:开启血氧测量；0x00:开启心率测量], [0x00:关闭；0x01:开启]
        sendCommand2(byteArrayOf(0x1A, 0x00, 0x01))
//        return mHeartRateSubject
        return manualHeartRateSPO2
    }

    fun startSpo2Detect(): BehaviorSubject<HeartRateSPO2> {
        //[type], [0x01:开启血氧测量；0x00:开启心率测量], [0x00:关闭；0x01:开启]
        sendCommand2(byteArrayOf(0x1A, 0x01, 0x01))
//        return mSpo2Subject
        return manualHeartRateSPO2
    }

    fun getBatterySubject(): BehaviorSubject<BatteryUsage> {
        return batteryUsage
    }
    fun getStorageSubject(): BehaviorSubject<StorageUsage> {
        return storageUsage
    }

    fun getBatteryStatue(): BehaviorSubject<BatteryUsage>{
        sendCommand2(byteArrayOf(0x21, 0x01))
        return batteryUsage
    }

    fun getStorageStatue(): BehaviorSubject<StorageUsage>{
        sendCommand2(byteArrayOf(0x21, 0x01))
        return storageUsage
    }
    fun startExerciseMode(accRange: Int, gyroRange: Int, geoRange: Int, rate: Int): BehaviorSubject<RawIMUData> {
        mIMUSubject = BehaviorSubject.create()

        Log.d(TAG, "startExerciseMode +++")
        sendCommand(ByteArray(9) {
            if (it == 0) 0x06
            else if (it in 1..2) Int2ByteArray(accRange)[it - 1]
            else if (it in 3..4) Int2ByteArray(gyroRange)[it - 3]
            else if (it in 5..6) Int2ByteArray(geoRange)[it - 5]
            else if (it in 7..8) Int2ByteArray(rate)[it - 7]
            else 0
        })
        return mIMUSubject
    }

    private fun Int2ByteArray(value: Int): ByteArray {
        return byteArrayOf(
            value.shr(8).and(0xff).toByte(),
            value.shr(0).and(0xff).toByte()
        )
    }

    private fun Long2ByteArray(value: Long): ByteArray {
        return byteArrayOf(
            value.shr(24).and(0xff).toByte(),
            value.shr(16).and(0xff).toByte(),
            value.shr(8).and(0xff).toByte(),
            value.shr(0).and(0xff).toByte()
        )
    }

    fun stopExerciseMode(): Completable {
        Log.d(TAG, "stop ExerciseMode +++")
        sendCommand(byteArrayOf(0x03))
        mIMUSubject.onComplete()

        return Completable.complete()
    }

    fun stopHeartRateDetect() {
        Log.d(TAG, "stop HeartRateDetect ---")
        sendCommand2(byteArrayOf(0x1A, 0x00, 0x00))
    }

    fun stopSpo2Detect() {
        Log.d(TAG, "stop HeartRateDetect ---")
        sendCommand2(byteArrayOf(0x1A, 0x01, 0x00))
    }


    /**
     *=========================================================================================================
     * 獲取睡眠數據  1B
     *
     */

    val mSleepReportManager = SleepReportManager(mBluetoothDevice.address, this)

    fun getSleepReport(opretion: Int): Single<MutableList<SleepReportManager.SleepReportBean>> {
        mSleepReportManager.reset()
        return Single.create {
            when (opretion) {
                0 -> {
                    mSleepReportManager.setCallback(it)
                    sendCommand2(byteArrayOf(0x1B, 0x01))
                }
                99 -> {  //方便刪除，所以再把RX開起來
                    mSleepReportManager.deleteDB()
                }
            }
        }
    }

    /**
     *
     * 獲取運動數據  1C
     *
     */

    val mWorkoutReportManager = WorkoutReportManager(mBluetoothDevice.address, this)

    fun getWorkoutReport(operation: Int): Single<List<ExerciseRecord>> {
        mWorkoutReportManager.reset()
        return Single.create {
            when (operation) {
                0 -> {
                    mWorkoutReportManager.setCallback1(it)
                    sendCommand2(byteArrayOf(0x1C, 0x01))
                }
                99 -> {
                    mWorkoutReportManager.deleteDB()
                }
            }
        }
    }

    fun getWorkoutStatus(): Single<Boolean> {
        return Single.create {
            mWorkoutReportManager.setCallback2(it)
            sendCommand2(byteArrayOf(0x1C, 0x05, 0x01))
        }
    }

    fun startExercise(mode: Byte) {
        sendCommand2(byteArrayOf(0x1C, 0x05, 0x02, 0x01, mode))
    }

    fun stopCurrentExercise() {
        sendCommand2(byteArrayOf(0x1C, 0x05, 0x02, 0x00))
    }

    /**
     *
     * DB parse Utilities
     *
     */

    fun parseString2MutableListInt(string: String): MutableList<Int> {
        val outputList = mutableListOf<Int>()
        val list = string.replace("[", "").replace("]","").split(", ")
        for (data in list) {
            if (!data.equals("")) {
                outputList.add(data.toInt())
            }
        }
        return outputList
    }

    fun parseString2MutableListFloat(string: String): MutableList<Float> {
        val outputList = mutableListOf<Float>()
        val list = string.replace("[", "").replace("]","").split(", ")
        for (data in list) {
            if (!data.equals("")) {
                outputList.add(data.toFloat())
            }
        }
        return outputList
    }


    /**
     *
     * 獲取日常數據  1D
     *
     */

    val mDailyReportManager = DailyReportManager(mBluetoothDevice.address, this)

    fun DailyReportOperation(type: Int): Single<MutableList<DailyReportManager.DailyReportBean>> {
        Log.d(TAG, "type = $type")

        return Single.create {
            mDailyReportManager.reset()
            mDailyReportManager.setCallback(it)

            if (type == 0) {
                sendCommand2(byteArrayOf(0x1D, 0x01))  //當天
            } else if (type == 1) {
                sendCommand2(byteArrayOf(0x1D, 0x02))  //歷史紀錄
            } else if (type == 99) {
                mDailyReportManager.deleteDB()
            }
        }
    }

    /**
     * 綁定與登入相關
     */

    private lateinit var mLoginEmitter: SingleEmitter<LoginStatus>
    private lateinit var mPairEmitter: SingleEmitter<PairStatus>
    private lateinit var mUnpairEmitter: SingleEmitter<UnpairStatus>

    fun login(memberId: String): Single<LoginStatus> {
        val uniqueIdentification = Utils.md5(memberId)

        TimelessBandGlobalModule.timelessBandSetMemberId(memberId,false)
        TimelessBandGlobalModule.timelessBandHeartRateFilter(50,false)
        TimelessBandGlobalModule.timelessBandSpo2Filter(50,false)

        Log.d("uniqueIdentification", ": ${Utils.byteArrayToHexStrWithSpaceSplit(uniqueIdentification)}")
        return Single.create {
            mLoginEmitter = it
            val command = ByteBuffer.allocate(19)
            command.put(byteArrayOf(0x1E,0x05))
            command.put(uniqueIdentification)
            command.put(byteArrayOf(0x01))  //Android：0x01； IOS：0x02
            sendCommand2(command.array())
        }
    }

    fun pair(memberId: String): Single<PairStatus> {
        val uniqueIdentification = Utils.md5(memberId)
        Log.d("uniqueIdentification", ": ${Utils.byteArrayToHexStrWithSpaceSplit(uniqueIdentification)}")
        return Single.create {
            mPairEmitter = it
            val command = ByteBuffer.allocate(18)
            command.put(byteArrayOf(0x1E,0x01))
            command.put(uniqueIdentification)
            sendCommand2(command.array())
        }
    }

    fun unpiar(memberId: String): Single<UnpairStatus> {
        val uniqueIdentification = Utils.md5(memberId)
        Log.d("uniqueIdentification", ": ${Utils.byteArrayToHexStrWithSpaceSplit(uniqueIdentification)}")
        return Single.create {
            mUnpairEmitter = it
            val command = ByteBuffer.allocate(18)
            command.put(byteArrayOf(0x1E,0x04))
            command.put(uniqueIdentification)
            sendCommand2(command.array())
        }
    }

    private fun onReceivedLoginStatus(byteArray: ByteArray) {
        val operationCode: Int = byteArray[0].toInt().and(0xff)
        //callbackType = 1(綁定成功)  2(失敗:已有綁定) 3(超時) 4(登入成功) 5(识别码不匹配，登入失败) 6(未綁定) 7(解綁成功) 8(登入異常，再試一次)
        val callbackType = byteArray[1].toInt().and(0xff)
        Log.d("onReceivedLoginStatus"," address: ${mBluetoothDevice.address}, operationCode: ${operationCode}")
        when(operationCode) {
            0x01 -> {
                when(callbackType) {
                    0x01 -> { mPairEmitter.onSuccess(PairStatus.SUCCESS) }
                    0x02 -> { mPairEmitter.onSuccess(PairStatus.PAIRED_FAIL) }
                    0x03 -> { mPairEmitter.onSuccess(PairStatus.TIMEOUT_FAIL) }
                }
            }
//            0x03 -> { //強制
//                when(callbackType) {
//                    0x01 -> { mLoginSingleEmitter.onSuccess(1) }
//                    0x02 -> { mLoginSingleEmitter.onSuccess(3) }
//                }
//            }
            0x04 -> {
                when(callbackType) {
                    0x01 -> { mUnpairEmitter.onSuccess(UnpairStatus.SUCCESS) }
                    0x02 -> { mUnpairEmitter.onSuccess(UnpairStatus.NOT_MATCHED_ID) }
                    0x03 -> { mUnpairEmitter.onSuccess(UnpairStatus.NOT_PAIRED) }
                }
            }
            0x05 -> {
                val callbackType = byteArray[1].toInt().and(0xff)
                when(callbackType) {
                    0x01 -> {
                        mLoginEmitter.onSuccess(LoginStatus.SUCCESS)
                        initStatus()
                    }
                    0x02 -> { mLoginEmitter.onSuccess(LoginStatus.NOT_MATCHED_ID) }
                    0x03 -> { mLoginEmitter.onSuccess(LoginStatus.NOT_PAIRED) }
                    0x04 -> { mLoginEmitter.onSuccess(LoginStatus.FAIL) }
                }
            }
        }
    }
    /**
     * 24小時 心率歷史紀錄 一小時記錄一筆
     * Testing Coroutine way
     */
/*
    private val mHeartRatesManager = HeartRatesManager(mBluetoothDevice.address, this, mContext)

    private lateinit var heartRateSyncProgress: ObservableEmitter<Int>
    fun startToSyncHeartRates() {} // ToDo

    fun getHeartRateHistory(op : Int) : Single<MutableList<HeartRateData.HR>> {
        Log.d(TAG, "心率歷史 操作: $op")
        mHeartRatesManager.reset()
        return Single.create {
            when (op) {
                0 -> {
                    mHeartRatesManager.setCallback(it)
                    sendCommand2(byteArrayOf(0x24, 0x03))
//                    heartRateDetectListEmitter = it
//                    getHeartRateCurve24(0).subscribe({
//                        heartRateDetectList.addAll(it)
//                        getButtonHeartRate_Spo2DetectData(buttonDetectCallback, 1)
//                    }, {})
                }
                99 -> {
                    Log.e(TAG, "送出99刪除資料庫2")
//                    mHeartRatesManager.deleteDB()
                }
            }
        }
    }
*/

    val mHeartRateCurve24Manager = HeartRateCurve24Manager(mBluetoothDevice.address, this)
    private fun getHeartRateCurve24(operation: Int): Single<MutableList<HeartRateDetect>> {
        Log.d(TAG, "24H歷史 操作: $operation")
        mHeartRateCurve24Manager.reset()
        return Single.create {
            when (operation) {
                1 -> {
                    mHeartRateCurve24Manager.setCallback(it)
                    sendCommand2(byteArrayOf(0x24, 0x01))
                }
                3 -> {
                    mHeartRateCurve24Manager.setCallback(it)
                    sendCommand2(byteArrayOf(0x24, 0x03))
                }
                99 -> {
                    Log.e(TAG, "送出99刪除資料庫2")
                    mHeartRateCurve24Manager.deleteDB()
                }
            }
        }
    }

    /**
     *
     * 24小時 心率歷史模式設定
     *  操作碼: 0x01 獲取當前模式
     *         0x02 設置模式
     *  設置參數: -1: 關閉歷史心率功能
     *           0: 一秒鐘一個值
     *           6: 每六分鐘測量一次
     *  默認值為: -1
     */
    fun setHeartRateDetectionMode(mode: HeartRateMonitorStatus){
        //設定模式
        when(mode) {
            HeartRateMonitorStatus.PER_SIX_MINUTES -> sendCommand2(byteArrayOf(0x2D,0x02, mode.value.toByte()))
            HeartRateMonitorStatus.CLOSED -> sendCommand2(byteArrayOf(0x2D,0x02, -1))
        }
    }
    lateinit var getHeartRateCurve24Emitter: SingleEmitter<HeartRateMonitorStatus>

    fun getHeartRateDetectionMode(): Single<HeartRateMonitorStatus>{
        return Single.create {
            getHeartRateCurve24Emitter = it
            //抓取模式
            sendCommand2(byteArrayOf(0x2D,0x01))
        }
    }



    private lateinit var heartRateDetectListEmitter: SingleEmitter<MutableList<HeartRateDetect>>
    private var heartRateDetectList: MutableList<HeartRateDetect> = mutableListOf()

    /**
     * 取得全天候(歷史)心率(包含手動以及自動測量)
     */
    fun getHeartRateCurve24AndManual(operation: Int): Single<MutableList<HeartRateDetect>>{
        heartRateDetectList.clear()
        haveAutoIt = false
        haveButtonIt = false
        return Single.create{ totslIt ->
            when (operation){
                0 -> {
                    // 將所有心律內容設置發送器
                    heartRateDetectListEmitter = totslIt
                    Timer().schedule(0,4000) {
                        if (haveAutoIt == false) {
                            // 註冊心律接收器
                            getHeartRateCurve24(1).subscribe ({ autoIt ->
                                // 存入心律列表
                                heartRateDetectList.addAll(autoIt)
                                // 註冊手動心律接收器
                                haveAutoIt = true
                            }, {})
                        } else if (haveAutoIt && haveButtonIt == false){
                            getHeartRateCurve24(3).subscribe ({ buttonIt ->
                                // 存入手動心律列表
                                heartRateDetectList.addAll(buttonIt)
                                haveButtonIt = true
                            }, {})
                        } else {
                            val list = heartRateDetectList.sortedBy { it.time }.toMutableList()
                            totslIt.onSuccess(list)
                            this.cancel()
                        }
                    }

                }
                99 -> {
                    Log.e(TAG, "送出99刪除資料庫1")
                    getHeartRateCurve24(99).subscribe()
                }
            }
        }
    }

    //手動測量callback
    val buttonDetectCallback = object : ButtonDetectCallback {
        override fun ButtonDetectCall(heartRateDetect: MutableList<HeartRateDetect>) {
            heartRateDetect.let {
                heartRateDetectList.addAll(it)
                val sortedArray = heartRateDetectList.sortedBy { it.time }.toMutableList()
                heartRateDetectListEmitter.onSuccess(sortedArray)
            }
        }
    }

    /**
     *
     * 按键 心率 / 血氧 数据获取
     *
     */

    private val mButtonDetectReportManager = ButtonDetectReportManager(mBluetoothDevice.address, this)

    fun getButtonHeartRate_Spo2DetectData(buttonDetectCallback: ButtonDetectCallback, operation: Int) {
        Log.d("WTRC", "getButtonHeartRate_Spo2DetectData (0x28) is disabled")
        mButtonDetectReportManager.reset()
        mButtonDetectReportManager.setCallback(buttonDetectCallback)
        when (operation) {
            1 -> {
                sendCommand2(byteArrayOf(0x28, 0x01)) // 心率
            }
            2 -> {
                sendCommand2(byteArrayOf(0x28, 0x02)) // 血氧
            }
            99 -> {
                //刪除db 心率 資料
                mButtonDetectReportManager.deleteDB("1")
            }
            100 -> {
                //刪除db 血氧 資料
                mButtonDetectReportManager.deleteDB("2")
            }
        }
    }

    /**
     *
     * LED 燈光控制
     *
     */

    fun setLED_RGB_Operatioin(red: Int, green: Int, blue: Int) {
        val Red = LEDTypeTransformation(red)
        val Green = LEDTypeTransformation(green)
        val Blue = LEDTypeTransformation(blue)
        sendCommand2(byteArrayOf(0x29, 0x00, Red, Green, Blue))

    }

    private fun LEDTypeTransformation(type: Int): Byte {
        when (type) {
            0 -> { return 0x00}
            1 -> { return 0x01}
            2 -> { return 0x02}
        }
        return 0x00
    }


    /**
     *     OTA
     **/

    private lateinit var startOtaObservableEmitter: ObservableEmitter<Int>

    fun startOTA(path: String): Observable<Int> {
        // quick fix, UI/UX should make sure the path is available.
        try { suotaManager.file = getByFirstFileName(path) }
        catch (e: Exception) {
            Log.e(TAG, "Error: $e")
            return  Observable.just(-1)
        }

        isOTA = true
        suotaManager.setGatt(mBluetoothGatt)
        suotaManager.device = mBluetoothDevice
        suotaManager.initParameterSettings()
        suotaManager.setContext(sContext)

        return Observable.create {
            startOtaObservableEmitter = it
            suotaManager.setOtaObservableEmitter(startOtaObservableEmitter)

            suotaManager.processStep(0) // ota init
            Completable.timer(2, TimeUnit.SECONDS).subscribe { suotaManager.processStep(1) }   // start ota
        }
    }

    /**
     *
     * 手環基本狀態
     *
     */
    fun getSystemStatus(){
        sendCommand2(byteArrayOf(0x15))
    }

    private fun onReceviedGeneralStatus(data: ByteArray) {
        fun parseExerciseType(type: Int): Exercise.ExerciseMode {
            var mode = Exercise.ExerciseMode.NONE
            when(type) {
                0x1F -> {
                    mode = Exercise.ExerciseMode.ELLIPTICAL_TRAINER
                }
                0x26 -> {
                    mode = Exercise.ExerciseMode.ROWING_MACHINE
                }
                0x15 -> {
                    mode = Exercise.ExerciseMode.WALKING
                }
                0x17 -> {
                    mode = Exercise.ExerciseMode.RUNNING
                }
                0x21 -> {
                    mode = Exercise.ExerciseMode.INDOOR_BIKE
                }
                0x18 -> {
                    mode = Exercise.ExerciseMode.CYCLING
                }
                else -> {
                    mode = Exercise.ExerciseMode.UNDEFINED
                }
            }
            return mode
        }

        val byteBuffer = ByteBuffer.wrap(data).order(ByteOrder.BIG_ENDIAN)
        // index starts from 1
        val batteryPercentage = byteBuffer.get().toInt()
        val exerciseStatus = byteBuffer.get().toInt()
        val heartRateStatus = byteBuffer.get().toInt()
        val geoStatus = byteBuffer.get().toInt()
        val acclStatus = byteBuffer.get().toInt()
        val gyroStatus = byteBuffer.get().toInt()
        // index 7
        val fwVersionH = byteBuffer.get().toInt()
        val fwVersionM = byteBuffer.get().toInt()
        val fwVersionL = byteBuffer.get().toInt()
        val hwVersionH = byteBuffer.get().toInt()
        val hwVersionL = byteBuffer.get().toInt()
        val fwVersion = "$fwVersionH$fwVersionM$fwVersionL"
        val hwVersion = "$hwVersionH$hwVersionL"
        // index 12
        val exerciseType = parseExerciseType(byteBuffer.get().toInt().and(0xff))
        val exerciseStarted = byteBuffer.int
        val workingStatusValue = byteBuffer.short.toInt()
        val imuSamplingRate = byteBuffer.get().toInt()
        val exerciseStartedTime = { exerciseStarted: Int ->
            if (exerciseStarted > 0) { getTimestring(exerciseStarted) }
            else { "none" }
        }

        val isIMUModeTransmitting        = workingStatusValue.and(0x0001) == 1 //IMU數據是否正在傳輸
        val syncingExerciseRecords       = workingStatusValue.and(0x0002) == 1 //運動數據是否正在同步
        val syncingHeartRateRecords      = workingStatusValue.and(0x0004) == 1 //心率數據是否在同步
        val syncingSleepRecords          = workingStatusValue.and(0x0010) == 1 //睡眠數據是否在同步
        val syncingStepRecords           = workingStatusValue.and(0x0020) == 1 //步數數據是否在同步
        val isFirmwareUpgrading          = workingStatusValue.and(0x0040) == 1 //設備是正在ota升級
        val currentExerciseStatus        = Exercise.ExerciseStatus.values()[exerciseStatus]   //當前運動狀態
        val currentExerciseMode          = exerciseType                              //當前運動類型
        val currentExerciseStartUnixtime = exerciseStarted                           //當前運動開始時間

        val workingStatusInfo = WorkingStatus(
            isIMUModeTransmitting,
            syncingExerciseRecords,
            syncingHeartRateRecords,
            syncingSleepRecords,
            syncingStepRecords,
            isFirmwareUpgrading,
            currentExerciseStatus,
            currentExerciseMode,
            currentExerciseStartUnixtime)

        val statusMSG = "battery: $batteryPercentage, FW: $fwVersion, HW: $hwVersion, PPG: $heartRateStatus, \n" +
                "Accl: $acclStatus, Gyro: $gyroStatus, Magn: $geoStatus, DR: $imuSamplingRate \n" +
                "Exercise>> Satus: $currentExerciseStatus, Mode: $currentExerciseMode, Type: $exerciseType,\t" +
                "Started at: ${exerciseStartedTime(exerciseStarted)} \n" + "workingStatus: $workingStatus"
        Log.d("0x15", statusMSG )

        val sensorStatusInfo = SensorStatus(
            heartRateStatus,
            geoStatus,
            acclStatus,
            gyroStatus,
            imuSamplingRate
        )

        workingStatus.onNext(workingStatusInfo)
        sensorStatus.onNext(sensorStatusInfo)
        onConnectedDeviceFieldSubject.onNext(TimelessBandDeviceField.battery(batteryPercentage))
        onConnectedDeviceFieldSubject.onNext(TimelessBandDeviceField.firmwareVersion(fwVersion))
        currentExercise.onNext(CurrentExercise(currentExerciseStatus, exerciseType, Date(currentExerciseStartUnixtime.toLong())))

        if (batteryPercentage < 20) {
            lowBatteryUsage.onNext(BatteryUsage(3,batteryPercentage))
        }

    }

    /**
     *
     * 睡眠血氧开关
     *
     */

    private lateinit var mSleepSpo2StatusSingleEmitter: SingleEmitter<SleepSpo2MonitorStatus>
    fun getSleepSpo2Status(): Single<SleepSpo2MonitorStatus> {
        return Single.create {
            mSleepSpo2StatusSingleEmitter = it
            sendCommand2(byteArrayOf(0x2c, 0x01))
        }
    }

    fun setSleepStatus(sleep: Boolean) {
        if (sleep) {
            sendCommand2(byteArrayOf(0x2c, 0x02, 0x01, 0x00))
        } else {
            sendCommand2(byteArrayOf(0x2c, 0x02, 0x00, 0x00))
        }
    }

    fun setSleepSpo2Status(sop2: Boolean) {
        if (sop2) {
            sendCommand2(byteArrayOf(0x2c, 0x02, 0x01, 0x01))
        } else {
            sendCommand2(byteArrayOf(0x2c, 0x02, 0x01, 0x00))
        }
    }

    /**
     *
     * Get Last Up Time
     *
     */
    private lateinit var mLastUptimeSingleEmitter: SingleEmitter<String>
    fun getLastUptime(): Single<String> {
        return Single.create {
            mLastUptimeSingleEmitter = it
            sendCommand2(byteArrayOf(0xfd.toByte()))
        }
    }
    private fun onReceivedLastUptime(data: ByteArray) {
        val byteBuffer = ByteBuffer.wrap(data).order(ByteOrder.BIG_ENDIAN)
//        val byteString = String(byteBuffer.array(), charset("ISO-8859-1"))
        val lastUptimeStamp = byteBuffer.int
        val debugMSG = "Last Up Time at: ${lastUptimeStamp}, ${getTimestring(lastUptimeStamp)}, ${byteBuffer.array().toHex()}"
        Log.d(TAG, debugMSG)
        mLastUptimeSingleEmitter.onSuccess(debugMSG)
    }

    fun getTimestring(timestamp_s: Int) =
        Utils.getFormatDateTimeStr(timestamp_s.toLong(), "yyyy-MM-dd HH:mm:ss")
    // java.time.format.DateTimeFormatter.ISO_INSTANT.format(java.time.Instant.ofEpochSecond(timestamp_s.toLong()))
    fun ByteArray.toHex(): String = joinToString(separator = "-") { eachByte -> "%02x".format(eachByte) }
    fun Byte.toHex() = { b:Byte -> "0x%02x".format(b)}

    fun unbindDeleteDB() {
        mSleepReportManager.deleteDB()
        mWorkoutReportManager.deleteDB()
        mDailyReportManager.deleteDB()
        mHeartRateCurve24Manager.deleteDB()
        mButtonDetectReportManager.deleteDB("1")
        mButtonDetectReportManager.deleteDB("2")
    }

    fun startGattSync() {
        gattSyncStatus.onNext(GattSyncStatus.STATE_SYNCHRONIZING)
        getSleepReport(0).subscribe({
            Log.d(TAG, "UnitleTest Step 3 startGattSync 睡眠報告 done")
            getWorkoutReport(0).subscribe({
                Log.d(TAG, "UnitleTest Step 3 startGattSync 運動報告 done")
                DailyReportOperation(1).subscribe({
                    Log.d(TAG, "UnitleTest Step 3 startGattSync 日常報告(1) done")
                    DailyReportOperation(0).subscribe({
                        Log.d(TAG, "UnitleTest Step 3 startGattSync 日常報告(0) done")
                        getHeartRateCurve24AndManual(0).subscribe({
                            Log.d(TAG, "UnitleTest Step 3 startGattSync 心律血氧報告 done")
                            gattSyncStatus.onNext(GattSyncStatus.STATE_IDLE)
                        },{})
                    },{})
                },{})
            },{})
        },{})
    }

    enum class LoginStatus { SUCCESS, NOT_PAIRED, NOT_MATCHED_ID, FAIL }
    enum class PairStatus { SUCCESS, TIMEOUT_FAIL, PAIRED_FAIL }
    enum class UnpairStatus { SUCCESS, NOT_MATCHED_ID, NOT_PAIRED }
    enum class SleepSpo2MonitorStatus { SLEEP_MONITOR_OFF, SLEEP_MONITOR_ON, SLEEP_SPO2_MONITOR_ON }

}

