package com.wondercise.wondercisetimelessband

import android.bluetooth.BluetoothDevice
import android.util.Log
import com.wondercise.device.FitnessDevice
import com.wondercise.device.HasFirmwareVersion
import com.wondercise.device.HasMacAddress
import com.wondercise.device.sensor.HeartMonitorSensorConfiguration
import com.wondercise.device.sensor.IMUConfiguration
import com.wondercise.device.sensor.RealtimeSensor
import com.wondercise.device.sensor.preprocess.imu.*
import com.wondercise.physdata.data.RealtimeHeartMonitorSample
import com.wondercise.physdata.data.RealtimeHeartRateSample
import com.wondercise.physdata.data.RealtimeIMUSample
import com.wondercise.physdata.spec.require.RequiredSensor
import com.wondercise.physdata.spec.support.SensorSupport
import com.wondercise.physdata.spec.support.SupportedSensor
import com.wondercise.wondercisetimelessband.database.*
import com.wondercise.wondercisetimelessband.suota.File
import com.wondercise.wondercisetimelessband.util.*
import io.reactivex.*
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import io.reactivex.subjects.ReplaySubject
import java.util.concurrent.TimeUnit

private const val TAG = "Timeless"

class TimelessBandDevice(id: String, private val mBluetoothDevice: BluetoothDevice, doConnectGatt: Boolean) :
    FitnessDevice(TimelessWonderciseBand, id, true), HasFirmwareVersion, HasMacAddress, TimelessBandBleHelper {

    override val brandModel: String
        get() = "WDC-SB"

    override val macAddress = DeviceField.MacAddress(mBluetoothDevice.address)

    override var firmwareVersion: DeviceField.FirmwareVersion? = null
        get() = field
        private set(value) {
            if (value == null) return
            field = value
            mDeviceFieldSubject.onNext(value)
        }

    override val thumbnailResourceId: Int
        get() = R.drawable.timelessband

    override val realtimeSensor: RealtimeSensor by lazy { TimelessBandRealtimeSensor() }

    companion object {
        val samplingRate = RequiredSensor.IMU.SamplingRate.rate100

        lateinit var mHeartRateDatabaseHandler:            HeartRateDatabaseHandler
        lateinit var mHeartRateVariabilityDatabaseHandler: HeartRateVariabilityDatabaseHandler
        lateinit var mSpo2DatabaseHandler:                 Spo2DatabaseHandler
        lateinit var mExerciseDatabaseHandler:             ExerciseDatabaseHandler
        lateinit var mStepDatabaseHandler:                 StepDatabaseHandler
        lateinit var mSleepDatabaseHandler:                SleepDatabaseHandler
    }

    private fun initHandler() {
        mHeartRateDatabaseHandler             = HeartRateDatabaseHandler(mTimelessBandBleAlgorithmHelper)
        mHeartRateVariabilityDatabaseHandler  = HeartRateVariabilityDatabaseHandler(mTimelessBandBleAlgorithmHelper)
        mSpo2DatabaseHandler                  = Spo2DatabaseHandler(mTimelessBandBleAlgorithmHelper)
        mExerciseDatabaseHandler              = ExerciseDatabaseHandler(mTimelessBandBleAlgorithmHelper)
        mStepDatabaseHandler                  = StepDatabaseHandler(mTimelessBandBleAlgorithmHelper)
        mSleepDatabaseHandler                 = SleepDatabaseHandler(mTimelessBandBleAlgorithmHelper)

        lowBatteryUsage               = mTimelessBandBleAlgorithmHelper.lowBatteryUsage
        lowStorageUsage               = mTimelessBandBleAlgorithmHelper.lowStorageUsage
        gattSyncStatus                = mTimelessBandBleAlgorithmHelper.gattSyncStatus
        imuModeTransmitting           = mTimelessBandBleAlgorithmHelper.imuModeTransmitting
        firmwareUpgrading             = mTimelessBandBleAlgorithmHelper.firmwareUpgrading
        currentExercise               = mTimelessBandBleAlgorithmHelper.currentExercise
        heartRateDetectObservable     = mTimelessBandBleAlgorithmHelper.heartRateDetectObservable
        spo2DetectObservable          = mTimelessBandBleAlgorithmHelper.spo2DetectObservable
        heartRateMonitoringObservable = mTimelessBandBleAlgorithmHelper.heartRateMonitoringObservable
        spo2MonitoringObservable      = mTimelessBandBleAlgorithmHelper.spo2MonitoringObservable
    }
    override var lowBatteryUsage: BehaviorSubject<BatteryUsage?>                 = BehaviorSubject.create()
    override var lowStorageUsage: BehaviorSubject<StorageUsage?>                 = BehaviorSubject.create()
    override var gattSyncStatus: BehaviorSubject<GattSyncStatus>                 = BehaviorSubject.createDefault(GattSyncStatus.STATE_IDLE)
    override var imuModeTransmitting: BehaviorSubject<Boolean>                   = BehaviorSubject.createDefault(false)
    override var firmwareUpgrading: BehaviorSubject<Boolean>                     = BehaviorSubject.createDefault(false)
    override var currentExercise: BehaviorSubject<CurrentExercise?>              = BehaviorSubject.create()
    override var heartRateDetectObservable: PublishSubject<HeartRateRecord>      = PublishSubject.create<HeartRateRecord>()
    override var spo2DetectObservable: PublishSubject<Spo2Record>                = PublishSubject.create<Spo2Record>()
    override var heartRateMonitoringObservable: PublishSubject<HeartRateRecord>  = PublishSubject.create<HeartRateRecord>()
    override var spo2MonitoringObservable: PublishSubject<Spo2Record>            = PublishSubject.create<Spo2Record>()

    override fun hasDeviceField(fieldName: DeviceField.FieldName): Boolean {
        return when (fieldName) {
            DeviceField.FieldName.battery -> true
            DeviceField.FieldName.signalStrength -> true
            DeviceField.FieldName.userIdentifier -> false
            DeviceField.FieldName.firmwareVersion -> true
            DeviceField.FieldName.macAddress -> true
        }
    }

    var mTimelessBandBleAlgorithmHelper = TimelessBandBleAlgorithmHelper(sContext, mConnectionStateSubject, mBluetoothDevice, doConnectGatt)
    override fun fetchDeviceField(fieldName: DeviceField.FieldName) {
        //TODO("Not yet implemented")
        when (fieldName) {
            DeviceField.FieldName.macAddress -> mDeviceFieldSubject.onNext(this.macAddress)
        }
    }

    private val mDisposables = CompositeDisposable()

    init {
        Log.d(TAG, "TimelessBandDevice init id:  " + id)
        mDisposables.add(mTimelessBandBleAlgorithmHelper.onConnectedDeviceFieldSubject.subscribe {
            when (it) {
                is TimelessBandDeviceField.battery -> mDeviceFieldSubject.onNext(
                    DeviceField.Battery(it.batteryPercentage)
                )
                is TimelessBandDeviceField.rssi -> mDeviceFieldSubject.onNext(
                    DeviceField.SignalStrength(it.signalStrength)
                )
                is TimelessBandDeviceField.firmwareVersion -> firmwareVersion = DeviceField.FirmwareVersion(it.firmwareVersion)
            }
            initHandler()
        })
    }

    override fun release(): Completable {
        this.mDisposables.dispose()
        return mTimelessBandBleAlgorithmHelper.unpair()
            .andThen(super.release())
    }

    internal fun onBluetoothAdapterEnabled(enabled: Boolean) {
        mTimelessBandBleAlgorithmHelper.onBluetoothAdapterEnabled(enabled)
    }

    inner class TimelessBandRealtimeSensor : RealtimeSensor(), IMUConfiguration,
        HeartMonitorSensorConfiguration {

        private var mInputSamplingRate = RequiredSensor.IMU.SamplingRate.rate100
        private var mOutputSamplingRate = mInputSamplingRate

        private var mSamplingMode = RequiredSensor.IMU.SamplingMode.freeRun
        private var mPreprocessInput: PipeInput = RawSampleInput()

        private var mDidFirstSampleReceived = false

        private val mRangeSensitivities = mapOf(
            RequiredSensor.IMU.Measure.acceleration to setOf(
                SupportedSensor.IMU.MeasurementRangeSensitivity(
                    RequiredSensor.IMU.MeasurementRange.acceleration16g,
                    RequiredSensor.IMU.MeasurementSensitivity.acceleration2048
                )
            ),
            RequiredSensor.IMU.Measure.angularVelocity to setOf(
                SupportedSensor.IMU.MeasurementRangeSensitivity(
                    RequiredSensor.IMU.MeasurementRange.angularVelocity2000dps,
                    RequiredSensor.IMU.MeasurementSensitivity.angularVelocity16384
                )
            )
        )

        override val mTransmissionStateMachine =
            object : DataTransmissionStateMachine(mConnectionStateSubject) {
                override fun initialBegin(
                    currentState: RealtimeState,
                    previousState: RealtimeState
                ) {
                    mDisposables.add(mTimelessBandBleAlgorithmHelper.stopExerciseMode().subscribe())
                    Completable.timer(200, TimeUnit.MILLISECONDS).subscribe {
                        stopHeartRateDetect()
                    }
                    super.initialBegin(currentState, previousState)
                }

                override fun startingBegin(
                    currentState: RealtimeState,
                    previousState: RealtimeState
                ) {
                    startReset()
                    when ((currentState as RealtimeState.starting).cause) {
                        RealtimeState.Cause.application -> {
                            observeRealtimeData(mTimelessBandBleAlgorithmHelper.startExerciseMode(0,0,0,0))
                        }
                        RealtimeState.Cause.timeout -> {
                        }
                    }

                    super.startingBegin(currentState, previousState)
                }

                private fun startReset() {
                    mDidFirstSampleReceived = false
                    mPreprocessInput.reset()
                }

                private fun observeRealtimeData(observable: BehaviorSubject<RawIMUData>) {
                    mDisposables.add(observable
                        .observeOn(Schedulers.computation())
                        .subscribe({ imuRawData ->
                            emitEvent(TransmissionEvent.didDataReady)
                            dataReceived()

                            if (!mDidFirstSampleReceived) { //收到第一筆IMU，再打開心率
                                observeRealtimeHeartData(startHeartRateDetectBehavior())
                                mDidFirstSampleReceived = true
                            }

                            //Log.d(TAG, "arrival_Index= $arrival_Index,  輸出時間: $outPutTime")
                            mPreprocessInput.processRaw(
                                imuRawData.arrivalTime,
                                imuRawData.accelerationRawSample,
                                imuRawData.angularVelocityRawSample
                            )
                        },
                            {
                                it.printStackTrace()
                            }
                        )
                    )
                }

                private fun observeRealtimeHeartData(observable: Observable<HeartRateSPO2>) {
                    mDisposables.add(observable
                        .observeOn(Schedulers.computation())
                        .subscribe({
                            Log.d(TAG, "Realtime Heart Data= ${it.toString()} at ${it.timeString}")
                            realtimeDataListener?.onRealtimeSample(RealtimeHeartRateSample(it.unixtime*1000, it.heartRate.toUInt()))
                        },
                            {
                                it.printStackTrace()
                            }))
                }
            }

        override val sensorSupport: SensorSupport by lazy {
            SensorSupport(
                setOf(
                    SupportedSensor.IMU(
                        measures = setOf(
                            RequiredSensor.IMU.Measure.acceleration,
                            RequiredSensor.IMU.Measure.angularVelocity
                        ),
                        measurementRangeSensitivities = mRangeSensitivities,
                        measurementUnits = setOf(
                            RequiredSensor.IMU.MeasurementUnit.standard,
                            RequiredSensor.IMU.MeasurementUnit.raw
                        ),
                        samplingRates = setOf(RequiredSensor.IMU.SamplingRate.rate100),
                        realtimeLevelCondition = SupportedSensor.IMU.RealtimeLevelCondition.alwaysHigh
                    ),
                    SupportedSensor.HeartMonitorSensor(
                        measures = setOf(
                            RequiredSensor.HeartMonitorSensor.Measure.heartRate
                        )
                    )
                )
            )
        }

        override fun configureBegin() {
        }

        override fun configureEnd(): Completable {
            when (mSamplingMode) {
                RequiredSensor.IMU.SamplingMode.freeRun -> {
                    if (mInputSamplingRate != mOutputSamplingRate) {
                        mPreprocessInput.next(Subsampler.AverageSubsampler(mInputSamplingRate.frequency / mOutputSamplingRate.frequency))
                    } else {
                        mPreprocessInput
                    }
                }
                RequiredSensor.IMU.SamplingMode.steadyAlign -> {
                    mPreprocessInput.next(TimestampBasedComplementer(mOutputSamplingRate))
                }
            }.next(
                LeftToRightHandedCoordinateSystemConverter(
                    arrayOf(
                        LeftToRightHandedCoordinateSystemConverter.CoordinateSystem.acceleration,
                        LeftToRightHandedCoordinateSystemConverter.CoordinateSystem.angularVelocity
                    )
                )
            )
            .next(object : ProcessPipe {
                override fun process(realtimeIMUSample: RealtimeIMUSample) {
                    if (mTransmissionStateMachine.isStarted) {
                        realtimeDataListener?.onRealtimeSample(realtimeIMUSample)
                    }
                }

                    override fun reset() {}
                })

            return Completable.complete()
        }

        override fun enableIMUMeasures(measures: Set<RequiredSensor.IMU.Measure>) {
        }

        override fun setMinimumMeasurementRange(
            measure: RequiredSensor.IMU.Measure,
            measurementRange: RequiredSensor.IMU.MeasurementRange
        ) {

        }

        override fun setMinimumMeasurementSensitivity(
            measure: RequiredSensor.IMU.Measure,
            measurementSensitivity: RequiredSensor.IMU.MeasurementSensitivity
        ) {

        }

        override fun setMeasurementUnit(measurementUnit: RequiredSensor.IMU.MeasurementUnit) {
            mPreprocessInput = when (measurementUnit) {
                RequiredSensor.IMU.MeasurementUnit.raw -> RawSampleInput()
                RequiredSensor.IMU.MeasurementUnit.standard -> RawSampleNormalizer(
                    arrayOf(
                        mRangeSensitivities.getValue(RequiredSensor.IMU.Measure.acceleration).first().measurementSensitivity,
                        mRangeSensitivities.getValue(RequiredSensor.IMU.Measure.angularVelocity).first().measurementSensitivity
                    )
                )
            }
        }

        override fun setSamplingRate(samplingRate: RequiredSensor.IMU.SamplingRate) {
            Log.d("wcl", "SamplingRate: " + samplingRate)
        }

        override fun setSamplingMode(samplingMode: RequiredSensor.IMU.SamplingMode) {
            mSamplingMode = samplingMode
        }

        override fun enableHeartMeasures(measures: Set<RequiredSensor.HeartMonitorSensor.Measure>) {
        }

    }

    /**
     * startGattSync()
     * @return Completable
     */
    override fun startGattSync(): Completable {
        // 將資料存到DB
        mTimelessBandBleAlgorithmHelper.startGattSync()
        return Completable.complete()
    }

    /**
     * setUserInfoData()
     * @param userInfoData
     * @return Completable
     */
    override fun setUserInfoData(userInfoData: UserInfoData): Completable {
        // UserInfo將資料存到DB
        mTimelessBandBleAlgorithmHelper.setUserInformation(userInfoData)
        Log.d(TAG, "UnitleTest Step 4 setUserInfoData userInfoData: $userInfoData")
        return Completable.complete()
    }

    /**
     * getSleepSpo2MonitorStatus()
     * @return Single<SleepSpo2MonitorStatus>
     */

    override fun getSleepSpo2MonitorStatus(): Single<SleepSpo2MonitorStatus> {
        return Single.create {
            Log.d(TAG, "SleepSpo2MonitorStatus :$it")
            mTimelessBandBleAlgorithmHelper.getSleepSpo2Status()
        }
    }
    /**
     * setSleepStatus()
     * @param enabled
     * @return Completable
     */
    override fun setSleepStatus(enabled: Boolean): Completable {
        Log.d(TAG, "SleepStatus :$enabled")
        mTimelessBandBleAlgorithmHelper.setSleepStatus(enabled)
        return Completable.complete()
    }

    /**
     * setSleepSpo2Status()
     * @param enabled
     * @return Completable
     */
    override fun setSleepSpo2Status(enabled: Boolean): Completable {
        Log.d(TAG, "SleepSpo2Status :$enabled")
        mTimelessBandBleAlgorithmHelper.setSleepSpo2Status(enabled)
        return Completable.complete()
    }

    /**
     * startExercise()
     * @param exerciseMode
     * @return Completable
     */

    override fun startExercise(exerciseMode: Exercise.ExerciseMode): Completable {
        when(exerciseMode) {
            Exercise.ExerciseMode.NONE -> {
                mTimelessBandBleAlgorithmHelper.startExercise(0x00)
            }
            Exercise.ExerciseMode.ELLIPTICAL_TRAINER -> {
                mTimelessBandBleAlgorithmHelper.startExercise(0x1F)
            }
            Exercise.ExerciseMode.ROWING_MACHINE -> {
                mTimelessBandBleAlgorithmHelper.startExercise(0x26)
            }
            Exercise.ExerciseMode.WALKING -> {
                mTimelessBandBleAlgorithmHelper.startExercise(0x15)
            }
            Exercise.ExerciseMode.RUNNING -> {
                mTimelessBandBleAlgorithmHelper.startExercise(0x17)
            }
            Exercise.ExerciseMode.INDOOR_BIKE -> {
                mTimelessBandBleAlgorithmHelper.startExercise(0x21)
            }
            Exercise.ExerciseMode.CYCLING -> {
                mTimelessBandBleAlgorithmHelper.startExercise(0x18)
            }
        }
        Log.d(TAG, "startExercise :$exerciseMode")
        return Completable.complete()
    }

    /**
     * stopCurrentExercise()
     * @return Completable
     */
    override fun stopCurrentExercise(): Completable {
        Log.d(TAG, "stopCurrentExercise")
        mTimelessBandBleAlgorithmHelper.stopCurrentExercise()
        return Completable.complete()
    }

    /**
     * setHeartRateMonitorStatus()
     * @param heartRateMonitorStatus
     * @return Completable
     */
    override fun setHeartRateMonitorStatus(heartRateMonitorStatus: HeartRateMonitorStatus): Completable {
        //設定模式
        Log.d(TAG, "heartRateMonitorStatus: $heartRateMonitorStatus")
        mTimelessBandBleAlgorithmHelper.setHeartRateDetectionMode(heartRateMonitorStatus)
        return Completable.complete()
    }
    /**
     * getHeartRateHeartRateMonitorStatus()
     * @return Single<HeartRateMonitorStatus>
     */
    override fun getHeartRateMonitorStatus(): Single<HeartRateMonitorStatus> {
        return Single.create({
            mTimelessBandBleAlgorithmHelper.getHeartRateDetectionMode()
        })
    }

    /**
     * startHeartRateDetect()
     * @return Completable
     */

    fun startHeartRateDetectBehavior(): BehaviorSubject<HeartRateSPO2> {
        return mTimelessBandBleAlgorithmHelper.startHeartRateDetect()
    }

    override fun startHeartRateDetect(): Completable {
        mTimelessBandBleAlgorithmHelper.startHeartRateDetect()
        return Completable.complete()
    }

    /**
     * stopHeartRateDetect()
     * @return Completable
     */
    override fun stopHeartRateDetect(): Completable {
        Log.d(TAG, "stop HeartRateDetect ---")
        mTimelessBandBleAlgorithmHelper.sendCommand2(byteArrayOf(0x1A, 0x00, 0x00))
        return Completable.complete()
    }

    /**
     * startSpo2Detect()
     * @return Completable
     */
    override fun startSpo2Detect(): Completable {
        //[type], [0x01:开启血氧测量；0x00:开启心率测量], [0x00:关闭；0x01:开启]
        mTimelessBandBleAlgorithmHelper.sendCommand2(byteArrayOf(0x1A, 0x01, 0x01))
//        return mSpo2Subject
        return Completable.complete()
    }

    /**
     * stopSpo2Detect()
     * @return Completable
     */
    override fun stopSpo2Detect(): Completable {
        Log.d(TAG, "stop HeartRateDetect ---")
        mTimelessBandBleAlgorithmHelper.sendCommand2(byteArrayOf(0x1A, 0x01, 0x00))
        return Completable.complete()
    }

    /**
     * startFirmwareUpgrade()
     * @param filePath
     * @return Observable<Int>
     */
    override fun startFirmwareUpgrade(filePath: String): Observable<Int> {
        // quick fix, UI/UX should make sure the path is available.
        return mTimelessBandBleAlgorithmHelper.startOTA(filePath)
    }

    /**
     * getHeartRateDatabaseHandler()
     * @return HeartRateDatabaseHandler
     */

    override fun getHeartRateDatabaseHandler(): HeartRateDatabaseHandler {
        return mHeartRateDatabaseHandler
    }

    /**
     * getHeartRateVariabilityDatabaseHandler()
     * @return HeartRateVariabilityDatabaseHandler
     */

    override fun getHeartRateVariabilityDatabaseHandler(): HeartRateVariabilityDatabaseHandler {
        return mHeartRateVariabilityDatabaseHandler
    }

    /**
     * getSpo2DatabaseHandler()
     * @return Spo2DatabaseHandler
     */

    override fun getSpo2DatabaseHandler(): Spo2DatabaseHandler {
        return mSpo2DatabaseHandler
    }

    /**
     * getExerciseDatabaseHandler()
     * @return ExerciseDatabaseHandler
     */
    override fun getExerciseDatabaseHandler(): ExerciseDatabaseHandler {
        return mExerciseDatabaseHandler
    }

    /**
     * getStepDatabaseHandler()
     * @return StepDatabaseHandler
     */
    override fun getStepDatabaseHandler(): StepDatabaseHandler {
        return mStepDatabaseHandler
    }

    /**
     * getSleepDatabaseHandler()
     * @return SleepDatabaseHandler
     */
    override fun getSleepDatabaseHandler(): SleepDatabaseHandler {
        return mSleepDatabaseHandler
    }

    override fun startHeartRateMonitoring(): Completable {
        mTimelessBandBleAlgorithmHelper.startHeartRateDetect()
        return Completable.complete()
    }

    override fun stopHeartRateMonitoring(): Completable {
        mTimelessBandBleAlgorithmHelper.stopHeartRateDetect()
        return Completable.complete()
    }

    override fun triggerOneTimeHeartRateDetection(): Single<HeartRateRecord> {
        return Single.create({

        })
    }

    override fun startSpo2Monitoring(): Completable {
        mTimelessBandBleAlgorithmHelper.startSpo2Detect()
        return Completable.complete()
    }

    override fun stopSpo2Monitoring(): Completable {
        mTimelessBandBleAlgorithmHelper.stopSpo2Detect()
        return Completable.complete()
    }

    override fun triggerOneTimeSpo2Detection(): Single<Spo2Record> {
        return Single.create({

        })
    }



    fun setHeartRateRecordType(type: HeartRateRecord.HeartRateRecordType) {
        mTimelessBandBleAlgorithmHelper.mHeartRateRecordType = type
    }

    fun setSpo2RecordType(type: Spo2Record.Spo2RecordType) {
        mTimelessBandBleAlgorithmHelper.mSpo2RecordType = type
    }

    fun subscribeAll() {
        val step = "UnitleTest Step2 -"
        lowBatteryUsage.subscribe({
            Log.d(TAG,"$step lowBatteryUsage: $it")
        })
        lowStorageUsage.subscribe({
            Log.d(TAG,"$step lowStorageUsage: $it")
        })
        gattSyncStatus.subscribe({
            Log.d(TAG,"$step gattSyncStatus: $it")
        })
        imuModeTransmitting.subscribe({
            Log.d(TAG,"$step imuModeTransmitting: $it")
        })
        firmwareUpgrading.subscribe({
            Log.d(TAG,"$step firmwareUpgrading: $it")
        })
        currentExercise.subscribe({
            Log.d(TAG,"$step currentExercise: $it")
        })
        heartRateDetectObservable.subscribe({
            Log.d(TAG,"$step heartRateDetectObservable: $it")
        })
        spo2DetectObservable.subscribe({
            Log.d(TAG,"$step spo2DetectObservable: $it")
        })
        heartRateMonitoringObservable.subscribe({
            Log.d(TAG,"$step heartRateMonitoringObservable: $it")
        })
        spo2MonitoringObservable.subscribe({
            Log.d(TAG,"$step spo2MonitoringObservable: $it")
        })
    }
}