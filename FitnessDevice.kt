package com.wondercise.device

import com.wondercise.device.sensor.RealtimeSensor
import com.wondercise.physdata.spec.support.DeviceProfile
import com.wondercise.physdata.spec.require.DeviceRequirement
import com.wondercise.physdata.spec.DeviceType
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import java.lang.IllegalArgumentException

abstract class FitnessDevice(val brand: Brand, val id: String, val canUnpair: Boolean) {
    enum class ConnectionState {
        connecting, connected, disconnected, unpair
    }

    sealed class DeviceField(val fieldName: FieldName) {
        enum class FieldName {
            battery, signalStrength, userIdentifier, firmwareVersion, macAddress
        }

        // If batteryPercentage is -1, it is runtime unavailable. We cannot ensure until device connected
        data class Battery(val batteryPercentage: Int) : DeviceField(FieldName.battery) {
            override fun toString() = "$batteryPercentage%"
        }
        data class SignalStrength(val strength: Int) : DeviceField(FieldName.signalStrength) {
            override fun toString() = strength.toString()
        }
        sealed class UserIdentifier(val identifier: String): DeviceField(FieldName.userIdentifier) {
            override fun toString() = identifier
        }

        class UUID(uuid: String): UserIdentifier(uuid)

        // The colorHex is in #RRGGBB hex digits format
        class Color(colorHex: String): UserIdentifier(colorHex) {
            val parsedColor: Int = android.graphics.Color.parseColor(colorHex)
        }

        data class MacAddress(val address: String): DeviceField(FieldName.macAddress) {
            override fun toString() = address
        }

        data class FirmwareVersion(val version: String) : DeviceField(FieldName.firmwareVersion) {
            override fun toString() = version
        }

    }

    protected val mConnectionStateSubject =
        BehaviorSubject.createDefault(ConnectionState.disconnected)
    val connectionState: ConnectionState
        get() = mConnectionStateSubject.value ?: ConnectionState.disconnected

    protected val mDeviceFieldSubject = PublishSubject.create<DeviceField>()

    var deviceType: DeviceType = DeviceType.Left.WRIST_BAND

    var battery: DeviceField.Battery? = null
        get() = field
        protected set(value) {
            if (value == null) return
            field = value
            mDeviceFieldSubject.onNext(value)
        }


    abstract val brandModel: String
    val deviceProfile: DeviceProfile get() = DeviceProfile(deviceType, realtimeSensor.sensorSupport)

    abstract val thumbnailResourceId: Int

    abstract val realtimeSensor: RealtimeSensor


    fun observeConnectionState(): Observable<ConnectionState> =
        mConnectionStateSubject.distinctUntilChanged().observeOn(AndroidSchedulers.mainThread())

    abstract fun hasDeviceField(fieldName: DeviceField.FieldName): Boolean
    open fun observeDeviceField(vararg fieldNames: DeviceField.FieldName): Observable<DeviceField> {
        val fieldCopy =
            fieldNames.filter { hasDeviceField(it) }.toTypedArray()

        return mDeviceFieldSubject.filter {
            it.fieldName in fieldCopy
        }.observeOn(AndroidSchedulers.mainThread())
    }

    abstract fun fetchDeviceField(fieldName: DeviceField.FieldName)


    open fun release() =
        Completable.fromAction {
            mConnectionStateSubject.onNext(ConnectionState.unpair)
            mConnectionStateSubject.onComplete()
            mDeviceFieldSubject.onComplete()
            realtimeSensor.release()
        }


    open fun configureRealtimeSensor(deviceRequirement: DeviceRequirement): Completable {
        if (this.deviceType !in deviceRequirement.acceptableDeviceTypes) {
            return Completable.error(IllegalArgumentException("Device type $deviceType is not compatible to this requirement"))
        }
        return realtimeSensor.configure(deviceRequirement.sensorRequirement)
    }

}

interface HasFirmwareVersion {
    // The type is optional since it can be unavailable while the device is disconnected
    val firmwareVersion: FitnessDevice.DeviceField.FirmwareVersion?
}

interface HasUserIdentifier {
    // The type is optional since it can be unavailable while the device is disconnected
    val userIdentifier: FitnessDevice.DeviceField.UserIdentifier?
}

interface HasMacAddress {
    val macAddress: FitnessDevice.DeviceField.MacAddress
}