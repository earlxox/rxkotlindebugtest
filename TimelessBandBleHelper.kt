package com.wondercise.wondercisetimelessband

import android.bluetooth.BluetoothDevice
import android.content.Context
import androidx.core.content.contentValuesOf
import com.wondercise.device.FitnessDevice
import com.wondercise.wondercisetimelessband.database.*
import com.wondercise.wondercisetimelessband.util.*
import io.reactivex.Single
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject


//TimelessBandBleHelper定義出一些TimelessBandDevice必須開外部使用的Rx或function
//這樣做的好處是規範出所有實作此見面的類別，都照著此定義的接口去撰寫程式，在開發跟引用的過程都可以更明確清楚。
//網路上有查到一個不錯的說法，利用抽象化的介面築起一道或多道阻隔變化的防火牆

interface TimelessBandBleHelper {
    var lowBatteryUsage: BehaviorSubject<BatteryUsage?>
    var lowStorageUsage: BehaviorSubject<StorageUsage?>
    var gattSyncStatus: BehaviorSubject<GattSyncStatus>
    var imuModeTransmitting: BehaviorSubject<Boolean>
    var firmwareUpgrading: BehaviorSubject<Boolean>
    var currentExercise: BehaviorSubject<CurrentExercise?>
    var heartRateDetectObservable: PublishSubject<HeartRateRecord> // 0x1A
    // 回傳第一筆100的
    var spo2DetectObservable: PublishSubject<Spo2Record>

    var heartRateMonitoringObservable : PublishSubject<HeartRateRecord>
    var spo2MonitoringObservable : PublishSubject<Spo2Record>

    /**
     * startGattSync()
     * @return Completable
     */
    fun startGattSync(): Completable

    /**
     * setUserInfoData()
     * @param userInfoData
     * @return Completable
     */
    fun setUserInfoData(userInfoData: UserInfoData): Completable

    /**
     * getSleepSpo2MonitorStatus()
     * @return Single<SleepSpo2MonitorStatus>
     */
    fun getSleepSpo2MonitorStatus(): Single<SleepSpo2MonitorStatus>

    /**
     * setSleepStatus()
     * @param enabled
     * @return Completable
     */
    fun setSleepStatus(enabled: Boolean): Completable

    /**
     * setSleepSpo2Status()
     * @param enabled
     * @return Completable
     */
    fun setSleepSpo2Status(enabled: Boolean): Completable

    /**
     * startExercise()
     * @param exerciseMode
     * @return Completable
     */
    fun startExercise(exerciseMode: Exercise.ExerciseMode): Completable

    /**
     * stopCurrentExercise()
     * @return Completable
     */
    fun stopCurrentExercise(): Completable

    /**
     * setHeartRateMonitorStatus()
     * @param heartRateMonitorStatus
     * @return Completable
     */
    fun setHeartRateMonitorStatus(heartRateMonitorStatus: HeartRateMonitorStatus): Completable

    /**
     * getHeartRateHeartRateMonitorStatus()
     * @return Single<HeartRateMonitorStatus>
     */
    fun getHeartRateMonitorStatus(): Single<HeartRateMonitorStatus>

    /**
     * startHeartRateDetect()
     * @return Completable
     */
    fun startHeartRateDetect(): Completable

    /**
     * stopHeartRateDetect()
     * @return Completable
     */
    fun stopHeartRateDetect(): Completable

    /**
     * startSpo2Detect()
     * @return Completable
     */
    fun startSpo2Detect(): Completable

    /**
     * stopSpo2Detect()
     * @return Completable
     */
    fun stopSpo2Detect(): Completable

    /**
     * startFirmwareUpgrade()
     * @param filePath
     * @return Observable<Int>
     */
    fun startFirmwareUpgrade(filePath: String): Observable<Int>

    /**
     * getHeartRateDatabaseHandler()
     * @return HeartRateDatabaseHandler
     */
    fun getHeartRateDatabaseHandler(): HeartRateDatabaseHandler

    /**
     * getHeartRateVariabilityDatabaseHandler()
     * @return HeartRateVariabilityDatabaseHandler
     */
    fun getHeartRateVariabilityDatabaseHandler(): HeartRateVariabilityDatabaseHandler

    /**
     * getSpo2DatabaseHandler()
     * @return Spo2DatabaseHandler
     */
    fun getSpo2DatabaseHandler(): Spo2DatabaseHandler

    /**
     * getExerciseDatabaseHandler()
     * @return ExerciseDatabaseHandler
     */
    fun getExerciseDatabaseHandler(): ExerciseDatabaseHandler

    /**
     * getStepDatabaseHandler()
     * @return StepDatabaseHandler
     */
    fun getStepDatabaseHandler(): StepDatabaseHandler

    /**
     * getSleepDatabaseHandler()
     * @return SleepDatabaseHandler
     */
    fun getSleepDatabaseHandler(): SleepDatabaseHandler

    fun startHeartRateMonitoring(): Completable
    fun stopHeartRateMonitoring(): Completable
    fun triggerOneTimeHeartRateDetection(): Single<HeartRateRecord>

    fun startSpo2Monitoring(): Completable
    fun stopSpo2Monitoring(): Completable
    fun triggerOneTimeSpo2Detection(): Single<Spo2Record>

}
